.. :changelog:

History
-------

1.3.0 (2020-12-23)
++++++++++++++++++

* Move Customer related stuff to customer model
* Move Provider related stuff ti provider model

1.2.0 (2020-09-17)
++++++++++++++++++

* Addition of a parameter to authorize the update of the balance 

1.1.0 (2020-09-14)
++++++++++++++++++

* Add a settings to block call if credit limit is reached

1.0.0 (2020-05-21)
++++++++++++++++++

* update dependencies


0.9.0 (2018-12-10)
++++++++++++++++++

* First release on PyPI.
