=====
Usage
=====

To use pyfb-company in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_company.apps.PyfbCompanyConfig',
        ...
    )

Add pyfb-company's URL patterns:

.. code-block:: python

    from pyfb_company import urls as pyfb_company_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_company_urls)),
        ...
    ]
