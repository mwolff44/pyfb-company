============
Installation
============

At the command line::

    $ easy_install pyfb-company

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-company
    $ pip install pyfb-company
