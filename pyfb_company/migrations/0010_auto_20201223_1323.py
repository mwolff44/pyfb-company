# Generated by Django 2.2.17 on 2020-12-23 13:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pyfb_company', '0009_auto_20201223_1308'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='customer_balance',
        ),
        migrations.RemoveField(
            model_name='company',
            name='customer_balance_update',
        ),
        migrations.RemoveField(
            model_name='company',
            name='provider_balance_update',
        ),
        migrations.RemoveField(
            model_name='company',
            name='supplier_balance',
        ),
    ]
