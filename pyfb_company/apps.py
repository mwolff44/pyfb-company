# -*- coding: utf-8
from django.apps import AppConfig


class PyfbCompanyConfig(AppConfig):
    name = 'pyfb_company'
