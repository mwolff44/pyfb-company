#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client
from pyfb_company.models import Company, Customer, Provider, CompanyBalanceHistory


def create_company(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["address"] = "address"
    defaults["contact_name"] = "contact_name"
    defaults["contact_phone"] = "contact_phone"
    defaults["customer_balance"] = '1.5'
    defaults["supplier_balance"] = '1.5'
    defaults.update(**kwargs)
    return Company.objects.create(**defaults)


def create_customer(**kwargs):
    defaults = {}
    defaults["account_number"] = "account_number"
    defaults["credit_limit"] = '1.5'
    defaults["low_credit_alert"] = '1.5'
    defaults["max_calls"] = 1
    defaults["calls_per_second"] = 1
    defaults["customer_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Customer.objects.create(**defaults)


def create_provider(**kwargs):
    defaults = {}
    defaults["supplier_enabled"] = 1
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return Provider.objects.create(**defaults)


def create_companybalancehistory(**kwargs):
    defaults = {}
    defaults["amount_debited"] = '1.5'
    defaults["amount_refund"] = '1.5'
    defaults["customer_balance"] = '1.5'
    defaults["supplier_balance"] = '1.5'
    defaults["operation_type"] = "customer"
    defaults["external_desc"] = "external_desc"
    defaults["internal_desc"] = "internal_desc"
    defaults.update(**kwargs)
    if "company" not in defaults:
        defaults["company"] = create_company()
    return CompanyBalanceHistory.objects.create(**defaults)


class CompanyViewTest(TestCase):
    '''
    Tests for Company
    '''
    def setUp(self):
        self.client = Client()

    def test_list_company(self):
        url = reverse('pyfb-company:pyfb_company_company_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_company(self):
        url = reverse('pyfb-company:pyfb_company_company_create')
        data = {
            "name": "name",
            "address": "address",
            "contact_name": "contact_name",
            "contact_phone": "contact_phone",
            "customer_balance": '1.5',
            "supplier_balance": '1.5',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_company(self):
        company = create_company()
        url = reverse('pyfb-company:pyfb_company_company_detail', args=[company.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_company(self):
        company = create_company()
        data = {
            "name": "name",
            "address": "address",
            "contact_name": "contact_name",
            "contact_phone": "contact_phone",
            "customer_balance": '1.5',
            "supplier_balance": '1.5',
        }
        url = reverse('pyfb-company:pyfb_company_company_update', args=[company.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerViewTest(TestCase):
    '''
    Tests for Customer
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customer(self):
        url = reverse('pyfb-company:pyfb_company_customer_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customer(self):
        url = reverse('pyfb-company:pyfb_company_customer_create')
        data = {
            "account_number": "account_number",
            "credit_limit": '1.5',
            "low_credit_alert": '1.5',
            "max_calls": 1,
            "calls_per_second": 1,
            "customer_enabled": 1,
            "company": create_company().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customer(self):
        customer = create_customer()
        url = reverse('pyfb-company:pyfb_company_customer_detail', args=[customer.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customer(self):
        customer = create_customer()
        data = {
            "account_number": "account_number",
            "credit_limit": '1.5',
            "low_credit_alert": '1.5',
            "max_calls": 1,
            "calls_per_second": 1,
            "customer_enabled": 1,
            "company": 1,
        }
        url = reverse('pyfb-company:pyfb_company_customer_update', args=[customer.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderViewTest(TestCase):
    '''
    Tests for Provider
    '''
    def setUp(self):
        self.client = Client()

    def test_list_provider(self):
        url = reverse('pyfb-company:pyfb_company_provider_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_provider(self):
        url = reverse('pyfb-company:pyfb_company_provider_create')
        data = {
            "supplier_enabled": 1,
            "company": create_company().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_provider(self):
        provider = create_provider()
        url = reverse('pyfb-company:pyfb_company_provider_detail', args=[provider.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_provider(self):
        provider = create_provider()
        data = {
            "supplier_enabled": 1,
            "company": 1,
        }
        url = reverse('pyfb-company:pyfb_company_provider_update', args=[provider.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CompanyBalanceHistoryViewTest(TestCase):
    '''
    Tests for CompanyBalanceHistory
    '''
    def setUp(self):
        self.client = Client()

    def test_list_companybalancehistory(self):
        url = reverse('pyfb-company:pyfb_company_companybalancehistory_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_companybalancehistory(self):
        url = reverse('pyfb-company:pyfb_company_companybalancehistory_create')
        data = {
            "amount_debited": '1.5',
            "amount_refund": '1.5',
            "customer_balance": '1.5',
            "supplier_balance": '1.5',
            "operation_type": "customer",
            "external_desc": "external_desc",
            "internal_desc": "internal_desc",
            "company": create_company().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_companybalancehistory(self):
        companybalancehistory = create_companybalancehistory()
        url = reverse('pyfb-company:pyfb_company_companybalancehistory_detail', args=[companybalancehistory.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_companybalancehistory(self):
        companybalancehistory = create_companybalancehistory()
        data = {
            "amount_debited": '1.5',
            "amount_refund": '1.5',
            "customer_balance": '1.5',
            "supplier_balance": '1.5',
            "operation_type": "customer",
            "external_desc": "external_desc",
            "internal_desc": "internal_desc",
            "company": 1,
        }
        url = reverse('pyfb-company:pyfb_company_companybalancehistory_update', args=[companybalancehistory.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
